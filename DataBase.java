import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import org.json.JSONObject;

public class DataBase {
    protected static String absolutePath = FileSystems.getDefault().getPath("./ticketstore").toAbsolutePath().toString();

    static JSONObject getTable(String tableName){
        var ticketsString = readFile(tableName + ".txt");
        return new JSONObject( (ticketsString.length() <= 2) ? "{}" : ticketsString);
    }

    static int create(JSONObject object, String tableName){
        var ticketsObject = getTable(tableName);
        var id = ticketsObject.length()+1;
        while( ticketsObject.has( String.valueOf(id) ) ) id++; // make id unique;
        ticketsObject.put( String.valueOf(id) , object);
        writeFile( ticketsObject.toString() , tableName+".txt");
        return id;
    }

    static String read(int id, String tableName){
        var ticketsObject = getTable(tableName);
        return ticketsObject.has( String.valueOf(id) ) ? ticketsObject.getJSONObject( String.valueOf(id) ).toString() : "{}";
    }

    static void update(int id, JSONObject object, String tableName){
        var ticketsObject = getTable(tableName);
        ticketsObject.put( String.valueOf(id) , object);
        writeFile(ticketsObject.toString(), tableName+".txt");
    }

    static void delete(int id, String tableName){
        var ticketsObject = getTable(tableName);
        ticketsObject.remove( String.valueOf(id)); // TODO remember to delete relationship
        writeFile(ticketsObject.toString(), tableName+".txt");
    }

    static void seed(){
        var constNum = 19072001;
        var tickets = new File(DataBase.absolutePath, "tickets.txt");
        if (!tickets.isFile()){
            for(var i = 0; i < 10; i++){
                var ticket = new Ticket(2001 + 100*i, (constNum % (13+ 21*i)), "Ha Noi", (constNum % (21 + 13*i)), "Bach Khoa");
                create(ticket.toJsonObject(), "tickets");
            }
        }
    }

    protected static void writeFile(String str, String fileName) {
        try {
            var file = new File(DataBase.absolutePath, fileName);
            if (!file.isFile()) file.createNewFile();
            
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static String readFile(String fileName) {
        var file = new File(DataBase.absolutePath, fileName);
        byte[] bytes = new byte[((int) file.length())];
        String contents = "{}";
        try {
            if (!file.isFile()) file.createNewFile();
            FileInputStream fis = new FileInputStream(file);
            fis.read(bytes);
            contents = new String(bytes);
            fis.close();
            return contents;
        } catch (IOException e) {
            e.printStackTrace();
            writeFile("{}", fileName);
            return contents;
        }
    }
}
